int trig = 8; // 出力ピン
int echo = 9; // 入力ピン

void setup() {
  Serial.begin(9600);
  pinMode(trig,OUTPUT);
  pinMode(echo,INPUT);
}

void loop() {
  //////////////
  //気温の測定//
  //////////////    
  int ans , temp , tv ;
  // アナログ０番ピンからセンサー値を読込む
  ans = analogRead(0) ;
  // センサー値を電圧に変換する
  tv  = map(ans,0,1023,0,5000);
  // 電圧から温度に変換する
  temp = map(tv,300,1600,-30,100) ;
  // 値をパソコン(ＩＤＥ)に送る
  Serial.print(temp) ;
  Serial.println("度");

  //////////////
  //距離の測定//
  //////////////
  // 超音波の出力終了
  digitalWrite(trig,LOW);
  delayMicroseconds(1);
  // 超音波を出力
  digitalWrite(trig,HIGH);
  delayMicroseconds(11);
  // 超音波を出力終了
  digitalWrite(trig,LOW);
  // 出力した超音波が返って来る時間を計測
  int t = pulseIn(echo,HIGH);
  // 計測した時間と音速(気温を考慮)から反射物までの距離を計算
  float v = (331.5+0.6*temp)/10000; // 音速
  float distance = v*((float)t/2); // 距離
  // 計算結果をシリアル通信で出力
  Serial.print(distance);
  Serial.println(" cm");
  delay(500);
}
