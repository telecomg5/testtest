#include <ESP8266WiFi.h>
#include <Firebase.h>
#include <FirebaseArduino.h>
#include <FirebaseCloudMessaging.h>
#include <FirebaseError.h>
#include <FirebaseHttpClient.h>
#include <FirebaseObject.h>

#define FIREBASE_HOST "test-2061d.firebaseio.com"
#define FIREBASE_AUTH "Vyxho8z4Lzaot1NTPcQVGvs4w9J1MONmrZZlZsN9"

//ご自分のルーターのSSIDを入力してください
const char* ssid = "aterm-c41611-g";
//ご自分のルーターのパスワード
const char* password = "9aa3a930ef01c";

#define echoPin 12 // Echo Pin
#define trigPin 14 // Trigger Pin
double Duration = 0; //受信した間隔
double Distance = 0; //距離

void setup() {
  Serial.begin(115200);

  // 超音波センサー接続
  pinMode( echoPin, INPUT );
  pinMode( trigPin, OUTPUT );

  // connect to wifi.
  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_STA); // omajinai
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(10000);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());

  // setup firebase.
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);

}

void loop()
{
  digitalWrite(trigPin, LOW); 
  delayMicroseconds(2); 
  digitalWrite( trigPin, HIGH ); //超音波を出力
  delayMicroseconds( 10 ); //
  digitalWrite( trigPin, LOW );
  Duration = pulseIn( echoPin, HIGH ); //センサからの入力
  if (Duration > 0) {
    Duration = Duration/2; //往復距離を半分にする
    Distance = Duration*340*100/1000000; // 音速を340m/sに設定
  }

  // update value
  Firebase.setFloat("dist", Distance);
  // handle error
  if (Firebase.failed()) {
      Serial.print("setting /number failed:");
      Serial.println(Firebase.error());  
      return;
  }
  delay(100);

  // get value 
  Serial.print("dist: ");
  Serial.println(Firebase.getFloat("dist"));
  delay(100);

}
